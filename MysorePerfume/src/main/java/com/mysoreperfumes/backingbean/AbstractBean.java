package com.mysoreperfumes.backingbean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public class AbstractBean {

	protected void facesGlobalInfo(String text) {
		facesGlobalMessage(FacesMessage.SEVERITY_INFO, text);
	}
		
	protected void facesGlobalError(String text) {
		facesGlobalMessage(FacesMessage.SEVERITY_ERROR, text);
	}
	
	protected void facesComponentError(String text, String componentId) {
		facesComponentMessage(componentId, FacesMessage.SEVERITY_ERROR, text);
	}
	
	protected void facesGlobalMessage(FacesMessage.Severity severity,
			String text) {
		facesClientMessage(null, severity, text);
	}
	
	protected void facesComponentMessage(String componentId, FacesMessage.Severity severity,
			String text) {
		facesClientMessage(componentId, severity, text);
	}
	
	protected void facesClientMessage(String client,
			FacesMessage.Severity severity, String text) {
		FacesMessage msg = new FacesMessage(severity, text, null);
		FacesContext.getCurrentInstance().addMessage(client, msg);
	}
	
	protected RequestContext getRequestContext(){
		return RequestContext.getCurrentInstance();
	}
	
	protected void sendMessage(String mobileNo, String message, String authkey){
		
		String senderId = "MYSORE";
		
		String route="4";

		URLConnection myURLConnection=null;
		URL myURL=null;
		BufferedReader reader=null;

		//encoding message 
		String encoded_message=URLEncoder.encode(message);

		//Send SMS API
		String mainUrl="https://control.msg91.com/api/sendhttp.php?";

		//Prepare parameter string 
		StringBuilder sbPostData= new StringBuilder(mainUrl);
		sbPostData.append("authkey="+authkey); 
		sbPostData.append("&mobiles="+mobileNo);
		sbPostData.append("&message="+encoded_message);
		sbPostData.append("&route="+route);
		sbPostData.append("&sender="+senderId);

		//final string
		mainUrl = sbPostData.toString();
		try
		{
		    //prepare connection
		    myURL = new URL(mainUrl);
		    myURLConnection = myURL.openConnection();
		    myURLConnection.connect();
		    reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		    //reading response 
		    String response;
		    while ((response = reader.readLine()) != null) 
		    //print response 
		    System.out.println(response);
		    
		    //finally close connection
		    reader.close();
		} 
		catch (IOException e) 
		{ 
			e.printStackTrace();
		} 
		
	}
	
	protected String getAppendIndiaCode(Long phoneNo){
		String mobileNo = "91"+phoneNo.toString();
		return mobileNo;
	}

}
