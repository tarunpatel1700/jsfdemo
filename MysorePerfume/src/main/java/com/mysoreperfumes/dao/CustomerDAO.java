package com.mysoreperfumes.dao;
 
import java.util.List;


 


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mysoreperfumes.entity.Customer;


@Repository
public class CustomerDAO  {
    @Autowired
    private SessionFactory sessionFactory;
 
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
  
    public void addCustomer(Customer customer) {
        getSessionFactory().getCurrentSession().save(customer);
    }
   
    public void deleteCustomer(Customer customer) {
        getSessionFactory().getCurrentSession().delete(customer);
    }
 
   
    public void updateCustomer(Customer customer) {
        getSessionFactory().getCurrentSession().update(customer);
    }
 
  
    public Customer getCustomerById(int id) {
        List list = getSessionFactory().getCurrentSession()
                                            .createQuery("from com.mysoreperfumes.entity.Customer  where id=?")
                                            .setParameter(0, id).list();

        return (Customer)list.get(0);
    }	
    
    public Customer getCustomerBySelfCode(String selfCode) {
        List list = getSessionFactory().getCurrentSession()
                                            .createQuery("from com.mysoreperfumes.entity.Customer  where selfcode=?")
                                            .setParameter(0, selfCode).list();
        if(list != null && list.size() > 0){
        	return (Customer)list.get(0);
        }else{
        	return null;
        }
        
    }
 
    public List<Customer> getCustomers() {
        List list = getSessionFactory().getCurrentSession().createQuery("from com.mysoreperfumes.entity.Customer").list();
        return list;
    }
 
}